const { expect } = require("chai");
const hre = require("hardhat");
const { loadFixture } = require("@nomicfoundation/hardhat-network-helpers");

describe("Token Manager", function () {

    // fixtures
    async function deployContractsFixture() {
      // Contracts are deployed using the first signer/account by default
      const [owner, bob] = await ethers.getSigners();

      const TokenA = await hre.ethers.getContractFactory("TokenA");
      const tokenA = await TokenA.deploy(100000000000000, "Token A", 10, "TKA");

      await tokenA.deployed()
  
      const TokenManager = await hre.ethers.getContractFactory("TokenManager");
      const tokenManager = await TokenManager.deploy();

      await tokenManager.deployed()

  
      return { tokenA, tokenManager, owner, bob };
    }

  it("Should send 200 amount of token A from contract to bob's account", async function () {
    const { tokenA, tokenManager, bob } = await loadFixture(deployContractsFixture);
    
    // transfer 1000 tokens to the contract
    tokenA.transfer(tokenManager.address, 1000);
    await tokenManager.connect(bob).getToken(tokenA.address, 200, {value: 200})

    // assert that the balance is correct
    let balance = await tokenA.balanceOf(bob.address);
    expect(balance).to.equal(200);
  });

  it("Should withdraw 200 amount of token A from owner account", async function () {
    const { tokenA, tokenManager, owner , bob } = await loadFixture(deployContractsFixture);

    // allow contract to remove 200 tokens from owner account
    await tokenA.connect(owner).approve(tokenManager.address, 200)
    await tokenManager.connect(bob).withdrawToken(tokenA.address, owner.address, 200)


    // assert that the balance is correct
    let balance = await tokenA.balanceOf(tokenManager.address);
    expect(balance).to.equal(200);
  });

  it("Should send 200 amount of token A from owner account to bob's account", async function () {
    const { tokenA, tokenManager, owner, bob } = await loadFixture(deployContractsFixture);

    // allow contract to remove 200 tokens from owner account
    await tokenA.connect(owner).approve(tokenManager.address, 200)
    await tokenManager.connect(bob).transferToken(tokenA.address, owner.address, bob.address, 200)


    // assert that the balance is correct
    let balance = await tokenA.balanceOf(bob.address);
    expect(balance).to.equal(200);
  });
});