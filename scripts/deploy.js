// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const { ethers } = require("hardhat");
const hre = require("hardhat");
const fs = require("fs");

async function main() {
  const [deployer] = await ethers.getSigners();
  console.log("Deploying contract with account:", deployer.address);
  console.log("Account balance:", (await deployer.getBalance()).toString());

  // deploy token A
  console.log("Deploying token A");
  const TokenA = await hre.ethers.getContractFactory("TokenA");
  const tokenA = await TokenA.deploy(100000000000000000000, "Token A", 18, "TKA");
  await tokenA.deployed();
  console.log("Token A address:", tokenA.address);

  // deploy token manager
  console.log("Deploying token manager");
  const TokenManager = await hre.ethers.getContractFactory("TokenManager");
  const tokenManager = await TokenManager.deploy();
  await tokenManager.deployed();
  console.log("Token manager address:", tokenManager.address);

  // store deployment data
  const tokenData = {
    address: tokenA.address,
    abi: JSON.parse(tokenA.interface.format('json'))
  }

  const tokenManagerData = {
    address: tokenManager.address,
    abi: JSON.parse(tokenManager.interface.format('json'))
  }

  // write the json file on the disk
  fs.writeFileSync('./build/TokenA.json', JSON.stringify(tokenData));
  fs.writeFileSync('./build/TokenManager.json', JSON.stringify(tokenManagerData));
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
