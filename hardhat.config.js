require('dotenv').config();
const { MNEMONIC, INFURA_API_URL } = process.env;
require("@nomicfoundation/hardhat-toolbox");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  solidity: {
    version: "0.8.17",
    settings: {
      optimizer:{
        enabled: true,
        runs: 200
      }
    }
  },
  networks: {
    goerli: {
      url: INFURA_API_URL,
      accounts: [`0x${MNEMONIC}`]
    }
  }
};
