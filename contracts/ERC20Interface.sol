// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

interface ERC20 {
    // functions
    function totalSupply() external view returns(uint256);
    
    /// @param _tokenOwner The address from which the balance will be retrieved
    /// @return balance the balance
    function balanceOf(address _tokenOwner) external view returns (uint);
    
    /// @param _tokenOwner The address of the account owning tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @return remaining Amount of remaining tokens allowed to spent
    function allowance(address _tokenOwner, address _spender) external view returns (uint);
    
    /// @notice send `_tokens` token to `_to` from `msg.sender`
    /// @param _to The address of the recipient
    /// @param _tokens The amount of token to be transferred
    /// @return success Whether the transfer was successful or not
    function transfer(address _to, uint _tokens) external returns (bool);
    
    /// @notice `msg.sender` approves `_addr` to spend `_tokens` tokens
    /// @param _spender The address of the account able to transfer the tokens
    /// @param _tokens The amount of wei to be approved for transfer
    /// @return success Whether the approval was successful or not
    function approve(address _spender, uint _tokens) external returns (bool);
    
    /// @notice send `_tokens` token to `_to` from `_from` on the condition it is approved by `_from`
    /// @param _from The address of the sender
    /// @param _to The address of the recipient
    /// @param _tokens The amount of token to be transferred
    /// @return success Whether the transfer was successful or not
    function transferFrom(address _from, address _to, uint _tokens) external returns(bool);

    // events
    event Approval(address indexed _tokenOwner, address indexed _spender, uint _tokens);
    event Transfer(address indexed _from, address indexed _to, uint _tokens);
}
