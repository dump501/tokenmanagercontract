// SPDX-License-Identifier: MIT
pragma solidity ^0.8.17;

import "./ERC20Interface.sol";
import "./SafeMath.sol";

/// @title Token A token
/// @author Tsafack Djiogo Fritz Albin
/// @notice contain the Token A contract logic. this is for test purposes
contract TokenA is ERC20{
    
    using SafeMath for uint256;

    mapping (address => uint256) public balances;
    mapping (address => mapping (address=>uint256)) public allowed;
    uint256 public totalSupply;

    string public name;
    uint public decimals;
    string public symbol;

    constructor (uint256 _initialAmount, string memory _tokenName, uint _tokenDecimals, string memory _tokenSymbol){
        balances[msg.sender] = _initialAmount;
        totalSupply = _initialAmount;
        name = _tokenName;
        decimals = _tokenDecimals;
        symbol = _tokenSymbol;
    }

    function transfer(address _to, uint _tokens) public returns (bool){
        require(balances[msg.sender] >= _tokens, "Unsufficient balance");
        balances[msg.sender] = balances[msg.sender].sub(_tokens);
        balances[_to] = balances[_to].add(_tokens);
        emit Transfer(msg.sender, _to, _tokens);

        return true;
    }

    function transferFrom(address _from, address _to, uint _tokens) public returns(bool) {
        uint256 tokenAllowance = allowed[_from][msg.sender];
        require(balances[_from] >= _tokens && tokenAllowance >= _tokens, "token balance or allowance is lower than amount requested");
        balances[_from] = balances[_from].sub(_tokens);
        balances[_to] = balances[_to].add(_tokens);
        emit Transfer(_from, _to, _tokens);

        return true;
    }

    function balanceOf(address _tokenOwner) public view returns (uint) {
        return balances[_tokenOwner];
    }

    function approve(address _spender, uint _tokens) public returns (bool) {
        allowed[msg.sender][_spender] = _tokens;
        emit Approval(msg.sender, _spender, _tokens);

        return true;
    }

    function allowance(address _tokenOwner, address _spender) public view returns (uint) {
        return allowed[_tokenOwner][_spender];
    }
}
