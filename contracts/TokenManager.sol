// SPDX-License-Identifier: MIT
pragma solidity 0.8.17;

import "./ERC20Interface.sol";

/// @title TokenManger contract
/// @author Tsafack Djiogo Fritz Albin
/// @notice contains the logic to manages tokens
/// @dev contains the functions to manage tokens
contract TokenManager {

    // events

    /// @notice Transfer ethers equivalent amounts of tokens from contract to sender's account
    /// @param tokenAddress the token's address we want to work with
    /// @param amount the amount of token that we want to send to the contract
    function getToken(address tokenAddress, uint amount) public payable {
        ERC20 token = ERC20(tokenAddress);
        uint256 contractBalance = token.balanceOf(address(this));
        require(msg.value >= amount, "You need to send sufficient ether");
        require(amount <= contractBalance, "Not enough tokens in the reserve");
        token.transfer(msg.sender, amount);
    }

    /// @notice contract will withdraw tokens from an account. 
    ///         NB: Do no forget to approve this smart to withdraw token 
    /// @param tokenAddress the token's address we want to work with
    /// @param from the account from which the contract will withdraw tokens
    /// @param amount the amount of token that the contract will withdraw
    function withdrawToken(address tokenAddress, address from, uint amount) public{
        require(amount > 0, "You need to transfer at least some tokens");
        ERC20 token = ERC20(tokenAddress);
        uint256 allowance = token.allowance(from, address(this));
        require(allowance >= amount, "Check the token allowance");
        token.transferFrom(from, address(this), amount);
    }

    // transfer token from an account to another
    /// @notice Transfer tokens from one account to another
    /// @param tokenAddress the token's address we want to work with
    /// @param from the account from which the contract will withdraw tokens
    /// @param to the account where the contract will send tokens to
    /// @param amount the amount of token that the contract will send from from address to to address
    function transferToken(address tokenAddress, address from, address to, uint amount) public {
        require(amount > 0, "You need to transfer at least some tokens");
        ERC20 token = ERC20(tokenAddress);
        uint256 allowance = token.allowance(from, address(this));
        require(allowance >= amount, "Check the token allowance");
        token.transferFrom(from, address(this), amount);
        token.transfer(to, amount);
    }
}